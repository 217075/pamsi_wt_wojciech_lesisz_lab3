
#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <stack>
#include <deque>
using namespace std;

class stosik { //Stos na tablicy//
	int *tab = new int[rozmiar];
	int rozmiar=0;
public:
void setrozmiar(int size){
	rozmiar = size;
};
int getrozmiar() {
	return rozmiar;
}
int* gettab() {
	return tab;
}
void settab(int* temp) {
	tab = temp;
}
void settab() {
	tab = new int[rozmiar];
}
void settab(int size, int dana)
{
	tab[size] = dana;
}
virtual void addtab(int dana) final {
	int *temp = tab;
	tab = new int [rozmiar + 1];
	tab[0] = dana;
	if (rozmiar == 0) {
		rozmiar++;
		return;
	}
	for (int i = 1; i < rozmiar+1; ++i)
	{
		tab[i] = temp[i - 1];
	}
	rozmiar++;
}
void deltab() {
	if (rozmiar == 0) {
		cout << "PUSTY!" << endl;
		return;
	}
	cout << tab[0] << endl;
	int *temp = tab;
	tab = new int[rozmiar - 1];
	for (int i = 0; i < rozmiar - 1; ++i)
	{
		tab[i] = temp[i+1];
	}
	rozmiar--;
}
void showtab() {
	if (rozmiar == 0){
		cout << "PUSTY!" << endl; return;
}
	for (int i = 0; i < rozmiar; ++i)
		cout << "| " << tab[i] << " |" << endl;
	cout << endl;
}

void cleartab() {
	tab = new int[0];
	rozmiar = 0;
}
};

template<typename name>

class Stos {//STL//

	stack<name> stos;

public:

	void dodajobiekt(name obiekt) {
		stos.push(obiekt);
	}
	void usunobiekt() {
		if (stos.empty())
			cout << "STOS JEST PUSTY!" << endl;
		else {
			cout << stos.top() << endl;
			stos.pop();
		}
	}
	void clear() {
		if (stos.empty())
			cout << "STOS JEST PUSTY!" << endl;
		else
			while (!stos.empty())
				stos.pop();
	}
	void show() {
		if (stos.empty())
			cout << "STOS JEST PUSTY!"<< endl;
		else {
			stack<name> temp;

			while (!stos.empty()) {
				cout << stos.top() << endl;
				temp.push(stos.top());
				stos.pop();
			}

			while (!temp.empty()) {
				stos.push(temp.top());
				temp.pop();
			}

		}

	}
};

class Kolej //korzystajac z stl//
{
	deque<int> kolej;
public:
	
void dodajobiekt(int obiekt) {
	kolej.push_back(obiekt);
}
	   void usunobiekt() {
		   if (kolej.empty())
			   cout << "KOLEJKA PUSTA!" << endl;
		   else {
			   cout << kolej.front() << endl;
			   kolej.pop_front();
		   }
	   }
	   void clear() {
		   if (kolej.empty())
			   cout << "KOLEJKA PUSTA!" << endl;
		   else
			   while (!kolej.empty())
				   kolej.pop_front();
	   }
	   void show() {
		   if (kolej.empty())
			   cout << "KOLEJKA PUSTA!" << endl;
		   else {
			   deque<int> temp;

			   while (!kolej.empty()) {
				   cout << kolej.front() << endl;
				   temp.push_back(kolej.front());
				   kolej.pop_front();
			   }

			   while (!temp.empty()) {
				   kolej.push_back(temp.front());
				   temp.pop_front();
			   }

		   }

	   }
};


class kolejka //korzystajac z tabeli//
	: public stosik//dziedziczy funkcje po stosie, oprocz dodawania elementu//
{
public:
	

	void addkolejka(int dana) {
		int *temp = gettab();
		setrozmiar(getrozmiar() + 1);
		settab();
		setrozmiar(getrozmiar() - 1);
		settab(temp);
		settab(getrozmiar(),dana);
		setrozmiar(getrozmiar() + 1);
	}
};

int main()
{
	Kolej kolejk;
	kolejka kk;
	stosik st;
	Stos<int> nowy;
	int opcja, z;
	cout << " 1=Kolejka, 2=Stos" << endl;

	cin >> z;
	if (z == 1) {
		{
			do {
				cout << endl;
				cout << "Kolejka na tablicy:" << endl;
				kk.showtab();
				cout << endl << "1. Nowa wartosc w kolejce(stl)" << endl;
				cout << "2. Usun wartosc z kolejki(stl)" << endl;
				cout << "3. Wyczysc kolejke(stl)" << endl;
				cout << "4. Wyswietl kolejke(stl)" << endl;
				cout << "5. Nowa wartosc w kolejce(tab)" << endl;
				cout << "6. Usun wartosc z kolejki(tab)" << endl;
				cout << "7. Wyczysc kolejke(tab)" << endl;
				cout << "8. Wyswietl kolejke(tab)" << endl;
				cout << "0. koniec" << endl << endl;
				cout << "Opcja: ";
				cin >> opcja;
				cout << endl;
				switch (opcja)
				{
				case 0:break;//koniec
				case 1://push
				{
					cin >> z;
					kolejk.dodajobiekt(z);
					break;
				}
				case 2://pop
				{
					kolejk.usunobiekt();
					break;
				}
				case 3://clear

				{
					kolejk.clear();
					break;
				}
				case 4://show
				{
					kolejk.show();
					break;
				}
				case 5://push
				{
					cin >> z;
					kk.addkolejka(z);
					break;
				}
				case 6://pop
				{
					kk.deltab();
					break;
				}
				case 7://clear

				{
					kk.cleartab();
					break;
				}
				case 8://show
				{
					kk.showtab();
					break;
				}
				}
			} while (opcja != 0);
		}
	}
	else
	{
		{
			do {
				cout << endl;
				cout << "Stos tab:" << endl;
				st.showtab();
				cout << endl << "1. Nowa wartosc na stos(stl)" << endl;
				cout << "2. Usun wartosc z stosu(stl)" << endl;
				cout << "3. Wyczysc stos(stl)" << endl;
				cout << "4. Wyswietl stos(stl)" << endl;
				cout << "5. Nowa wartosc na stos(tab)" << endl;
				cout << "6. Usun wartosc z stosu(tab)" << endl;
				cout << "7. Wyczysc stos(tab)" << endl;
				cout << "8. Wyswietl stos(tab)" << endl;
				cout << "0. koniec" << endl << endl;
				cout << "Opcja: ";
				cin >> opcja;
				cout << endl;
				switch (opcja)
				{
				case 0:break;//koniec
				case 1://push
				{
					cin >> z;
					nowy.dodajobiekt(z);
					break;
				}
				case 2://pop
				{
					nowy.usunobiekt();
					break;
				}
				case 3://clear

				{
					nowy.clear();
					break;
				}
				case 4://show
				{
					nowy.show();
					break;
				}
				case 5://push
				{
					cin >> z;
					st.addtab(z);
					break;
				}
				case 6://pop
				{
					st.deltab();
					break;
				}
				case 7://clear

				{
					st.cleartab();
					break;
				}
				case 8://show
				{
					st.showtab();
					break;
				}
				}
			} while (opcja != 0);
		}
	}
}
/*

int main() //DO EKSPERYMENTU
{
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	kolejka kk;
	Kolej kol;
	Stos test;
	stosik st;
	int opcja, z;
	for (int j = 0; j < 10; j++) {
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);

		for (int i = 0; i < 1000; i++) {
			srand(time(NULL));
			st.addtab((rand() % 800000) + 500000);
		}

		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout << elapsedTime << " ms.\n";
	}
	cin >> z;
}

*/